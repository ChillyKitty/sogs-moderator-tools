# sogs-moderator-tools

Scripts, snippets, and tools to help Session moderators/admins


## bulk remove emoji reactions by session ID
```sqlite3 /var/lib/session-open-group-server/sogs.db "DELETE FROM user_reactions WHERE user = (SELECT id FROM users WHERE session_id = 'session_id_to_block_here')"```
